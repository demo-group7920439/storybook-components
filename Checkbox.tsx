import { ReactNode } from "react";
import { Check } from "../../icons";
import Typography from "../Typography";

type Checkbox = {
  title: string | ReactNode;
  textClass?: string;
  size?: "normal" | "medium";
  checked?: boolean;
  onClick?: () => void;
  disabled?: boolean;
};

const Checkbox = ({
  title,
  checked,
  textClass,
  disabled = false,
  size = "normal",
  onClick,
}: Checkbox) => {
  const handleClick = () => {
    if (!disabled) {
      onClick && onClick();
    }
  };

  const getBoxSize = () => {
    if (size === "normal") {
      return { width: "18px", padding: "1px", height: "18px" };
    } else {
      return { width: "24px", padding: "2px", height: "24px" };
    }
  };

  const getCheckSize = () => {
    if (size === "normal") return 14;
    else return 18;
  };

  return (
    <div
      className="flex justify-start items-center cursor-pointer"
      onClick={() => handleClick()}
    >
      <div
        className={`border-gray-300 text-white border transition-colors duration-300 text-center rounded ${
          checked && "bg-primary"
        }`}
        style={getBoxSize()}
      >
        <Check
          width={getCheckSize()}
          height={getCheckSize()}
          disabled={disabled}
        />
      </div>
      {title && (
        <div className="display-inline ml-3">
          <Typography>
            <span className={textClass || ""}>{title}</span>
          </Typography>
        </div>
      )}
    </div>
  );
};

export default Checkbox;
