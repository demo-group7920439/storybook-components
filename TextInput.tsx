import cn from "clsx";
import s from "./Input.module.css";
import React, { InputHTMLAttributes, useMemo, useState } from "react";

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  onChange?: (...args: any[]) => any;
}

interface ActionProps {
  text?: string;
  icon?: any;
  onAction: (e: string) => void;
  loading?: boolean;
  disabled?: boolean;
}

const TextInput: React.FC<
  InputProps & {
    variant?: "outlined" | "contained" | "shadow" | "gray" | "flat";
    label?: string;
    preElement?: any;
    action?: ActionProps;
    rows?: number;
    // setRef?: any;
    invalid?: boolean;
    hardInvalid?: boolean;
    ref?: any;
  }
> = (props) => {
  const {
    className,
    children,
    onChange,
    rows,
    invalid,
    // hardInvalid,
    value,
    action,
    variant = "outlined",
    ref,
    preElement = <></>,
    label,
    ...rest
  } = props;

  // const [dirty, setDirty] = useState(false)
  // const [hasValidatedOnce, setHasValidatedOnce] = useState(false)
  const rootClassName = cn(s.root, {
    [s.outlined]: variant === "outlined",
    [s.contained]: variant === "contained",
    [s.shadow]: variant === "shadow",
    [s.gray]: variant === "gray",
    [s.flat]: variant === "flat",
    [s.invalid]: variant === "outlined" && invalid,
  });

  const wrapperClass = cn(s.wrapperRoot, {
    [s.wrapperShadow]: variant === "shadow",
    [s.wrapperGray]: variant === "gray",
    [s.wrapperFlat]: variant === "flat",
    [s.wrapperShadowInvalid]: invalid && variant !== "outlined",
  });

  const [id] = useState(new Date().getTime().toString());
  const handleOnChange = (e: any) => {
    if (onChange) {
      onChange(e.target.value);
    }
    return null;
  };

  const inputProps: any = useMemo(() => {
    return {
      className: `${rootClassName} peer placeholder-slate-300`,
      onChange: handleOnChange,
      autoComplete: "off",
      id: id,
      autoCorrect: "off",
      autoCapitalize: "off",
      spellCheck: "false",
      placeholder: variant === "outlined" ? " " : rest.placeholder || " ",
      ...rest,
    };
  }, [rest, variant, id, rootClassName, handleOnChange]);
  const onKeyDown = (e: any) => {
    if (action?.onAction && !action?.disabled && !action.loading) {
      if (e.code === "Enter") {
        action.onAction(value as string);
      }
    }
  };
  return (
    <div className={wrapperClass}>
      <div className={"flex items-center"}>
        {preElement}
        {rows ? (
          <textarea
            // ref={(r) => (setRef ? setRef(r) : null)}
            ref={ref}
            value={value}
            onKeyDown={onKeyDown}
            {...inputProps}
            rows={rows}
          />
        ) : (
          <input
            {...inputProps}
            value={value}
            onKeyDown={onKeyDown}
            // ref={(r) => (setRef ? setRef(r) : null)}
            ref={ref}
          />
        )}
      </div>

      {label && (
        <label
          htmlFor={id}
          className={
            "absolute text-gray-500 duration-300 transform -translate-y-[1.2rem]  translate-x-2 scale-75 top-2 z-10 origin-[0] bg-white  px-2 peer-focus:px-2 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1 "
          }
        >
          {label}
        </label>
      )}
    </div>
  );
};

export default TextInput;
